FROM alpine:3.7
RUN apk add --update perl make curl g++ gcc wget perl-dev openssl openssl-dev \
        && curl -L https://cpanmin.us | perl - App::cpanminus \
        && cpanm LWP::UserAgent
RUN cpanm LWP::Simple
RUN cpanm LWP::Protocol
RUN cpanm LWP::Protocol::https